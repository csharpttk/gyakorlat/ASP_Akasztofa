﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Akasztófa.aspx.cs" Inherits="ASP_Akasztofa_Osztaly.Akasztófa" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body >
    
    <form id="form1" runat="server">
        
        <div style="text-align:center">
            <asp:Label ID="Label1" runat="server" Text="Akasztófa játék" Font-Size="XX-Large"></asp:Label>
            <br />
            <asp:Image ID="Akasztófa_képe" runat="server" ImageUrl="~/Képek/0.png" style="border-color:black;border-style:double;border-width:thick;border-radius:5px" /> 
            <br />
            <asp:textbox  ID="Közbülső_text" runat="server" Font-Size="Large" CssClass="form-control" ></asp:textbox>
            <asp:textbox  ID="Tipp_szöveg" runat="server" Font-Size="Large"> </asp:textbox>
             <br /> <br />
            <table style="text-align:center; margin-left:auto;margin-right:auto">
                <tr>
                    <td> 
                        <asp:Button ID="Újjátszma" runat="server" Text="Újrakezd!"  OnClick="Újjátszma_Click" Font-Size="Large" />

                    </td> 
                   
                    <td style="width:56%">
                       <asp:Label ID="Győz_veszít" runat="server" Font-Size="X-Large" ForeColor="Red" ></asp:Label>
                    </td>
                    <td> 
                        <asp:Button ID="Tipp_gomb" runat="server" Text="Tippeljen!"  OnClick="Tipp_gomb_Click" Font-Size="Large"  />
                    </td>
                </tr>
            </table>
           
            
            
       </div> 

    </form>
</body>
    
</html>
