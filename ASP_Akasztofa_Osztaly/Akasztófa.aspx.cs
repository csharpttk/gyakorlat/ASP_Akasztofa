﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ASP_Akasztofa_Osztaly
{
    public partial class Akasztófa : System.Web.UI.Page
    {
        private static Akasztófa_Leszármazott játék;
        private void inic()
        {               
                játék = new Akasztófa_Leszármazott();
                Akasztófa_képe.ImageUrl = játék.megjelenítendőfájl();
                Közbülső_text.Text = játék.közbülső;
                Tipp_szöveg.Text = "Ide kérem a tippet!";
                Győz_veszít.Text = "";
                Tipp_gomb.Enabled = true;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                 inic();               
            }
        }

        protected void Tipp_gomb_Click(object sender, EventArgs e)
        {
            if (Tipp_szöveg.Text.Length > 0) //írt-e valamit?
            {
                játék = (Akasztófa_Leszármazott)(játék + Tipp_szöveg.Text[0]);
                Akasztófa_képe.ImageUrl = játék.megjelenítendőfájl();
                Közbülső_text.Text = játék.közbülső;
                Tipp_szöveg.Text = "";
                if (játék.kitalálandó == játék.közbülső)
                {
                    Győz_veszít.Text = "Gratulálok!";
                    Tipp_gomb.Enabled = false;
                }
                if (játék.hibapont == 11)
                {
                    Győz_veszít.Text = "Sajnos vesztett!";
                    Tipp_gomb.Enabled = false;
                }
            }
        }

        protected void Újjátszma_Click(object sender, EventArgs e)
        {
            inic();
        }
    }
}