﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_Akasztofa_Osztaly
{
    public class Akasztófa_Alap
    {
        public  int hibapont { get; set; }
        public  string kitalálandó { get; set; }
        public  string közbülső { get; set; }
        private static string[] szavak = { "almafa", "programozás", "verzió", "tesztelés", "függvények", "ciklusok" };
        public Akasztófa_Alap(){
           
            Random r = new Random(); int melyik = r.Next(szavak.Length);
            kitalálandó = szavak[melyik];
            közbülső=new String('*', kitalálandó.Length);
            hibapont = 0;
        }
        public static Akasztófa_Alap operator +(Akasztófa_Alap állapot, char tipp)
        {   //új tipp érkezése megváltoztatja az állapotot
            Akasztófa_Alap újállapot = new Akasztófa_Alap()
            {
                hibapont = állapot.hibapont,
                kitalálandó = állapot.kitalálandó,
                közbülső = állapot.közbülső
            };
            string segéd = ""; //itt építjük fel az új közbülső eredményt
            for (int i = 0; i < újállapot.kitalálandó.Length; i++)
            {
                if (tipp == újállapot.kitalálandó[i]) { segéd += tipp; }//konkatenáció
                else { segéd += újállapot.közbülső[i]; }
            }
            if (újállapot.közbülső == segéd) újállapot.hibapont++;
            újállapot.közbülső = segéd;
            return újállapot;
        }
        
    }
}
